(function($) {

    $(function() {
    	// Replace standard SS link and logo with letsco
        $('.cms-logo a')
        	.attr('href', 'https://www.letsco.co/')
        	.removeAttr('title')
        	.css({'background': 'url(/mlbc/images/logo/letsco.ico) no-repeat center'});
    });

}(jQuery));