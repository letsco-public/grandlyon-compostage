(function($) {

    // Document ready
    $(function() {
    	console.log('Sitename custom JS is ready !');

        // Close cookies box (very top header) on click "ok".
        $('.cookies_close').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            jQuery('#very-top-bar').slideUp();
            createCookie('showVeryTopHeader', 0, 0);
        });
    });

}(jQuery));