<?php

/**
 * EdPCrmIdeaExtension
 * @since [JD-2022.06.09] #4559
 * @extends Event
 */
class CEvent extends DataExtension
{
    private static $db = [
        'ToodegoLink' => 'Varchar(255)',
    ];

    public function updateCMSFields(FieldList $fields)
    {
    	$fields->insertAfter(TextField::create('ToodegoLink', "Lien Toodego"), 'Type');
    }
}
