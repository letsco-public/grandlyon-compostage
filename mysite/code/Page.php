<?php

class Page extends MlbcPage
{
    private static $hide_ancestor = 'MlbcPage';

    public function getCMSFields()
    {
        $f = parent::getCMSFields();
        return $f;
    }
}

class Page_Controller extends MlbcController
{
    private static $allowed_actions = array(
    );

    public function init()
    {
        parent::init();

        // Requirements::css('mysite/css/custom-editor.css');
        Requirements::css('mysite/css/custom.css');
        Requirements::javascript('mysite/javascript/custom.js');

        // Activate subsites profile after main site requirements to override them by subsite's ones.
        //SubsiteProfile::enable();
    }
}
