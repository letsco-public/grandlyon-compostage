<?php

global $project;
$project = 'mysite';

global $database;
$database = ''; // Database is defined in _ss_environment.php

require_once('conf/ConfigureFromEnv.php');

if (Director::isLive()) {
	Director::forceSSL(); // Redirect all requests to https
	// Director::forceWWW(); // Redirect all requests to www to enable in runcloud.io
}

// Set the site locale
i18n::set_locale('fr_FR');

if (class_exists('MyCustomLeft')) {
    SS_Object::add_extension('LeftAndMain', 'MyCustomLeft');
}